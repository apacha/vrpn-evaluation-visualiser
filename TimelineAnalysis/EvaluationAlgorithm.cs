﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TimelineAnalysis.Objects;

namespace TimelineAnalysis
{
    public class EvaluationAlgorithm
    {
        public List<double> EvaluatePerformance(List<MotionEvent> vrpnTrackerEvents, List<MotionEvent> mobileEvents, Func<MotionEvent, MotionEvent, double> extractDistanceFunc)
        {
            return EvaluatePerformance(vrpnTrackerEvents, mobileEvents, extractDistanceFunc, double.MaxValue);
        }

        /// <summary>
        /// Takes two lists of motion-events and returns a list of distances between pairs
        /// </summary>
        /// <param name="vrpnTrackerEvents"></param>
        /// <param name="mobileEvents"></param>
        /// <param name="extractDistanceFunc">Function that takes two motion events and calculates a distance between them.</param>
        /// <param name="maxTime">Maximal time in seconds that should be evaluated</param>
        /// <returns></returns>
        public List<double> EvaluatePerformance(List<MotionEvent> vrpnTrackerEvents, List<MotionEvent> mobileEvents, Func<MotionEvent, MotionEvent, double> extractDistanceFunc, double maxTime)
        {
            if (extractDistanceFunc == null)
                return null;

            // Create a new list of tuples for all pairs that will be generated in the next steps
            List<double> pairs = new List<double>(vrpnTrackerEvents.Count);

            // The index for accessing elements in the mobileTrackerEvents-List
            int mobileEventIndex = 0;

            // Run through all elements of the vrpnTrackerEvents-List and find matching partners that are the same according 
            // to the corrected timestamp of mobile events. If no matching partner was found, 
            // find the closest two neighbours and interpolate between them.
            for (int vrpnEventIndex = 0; vrpnEventIndex < vrpnTrackerEvents.Count; vrpnEventIndex++)
            {
                // Skip elements that happened before the current element
                while (vrpnTrackerEvents[vrpnEventIndex].EventTime > mobileEvents[mobileEventIndex].CorrectedTime)
                {
                    mobileEventIndex++;

                    if (mobileEventIndex == mobileEvents.Count || vrpnTrackerEvents[vrpnEventIndex].EventTime > maxTime)
                    {
                        // End of list, no more elements available
                        return pairs;
                    }
                }

                // There has never been an element with a smaller timestamp
                if (mobileEventIndex == 0)
                {
                    // Skip first few elements
                    continue;
                }

                if (vrpnTrackerEvents[vrpnEventIndex].EventTime == mobileEvents[mobileEventIndex].CorrectedTime)
                { 
                    // Exact match found. Go to next element from vrpn list
                    pairs.Add(extractDistanceFunc(vrpnTrackerEvents[vrpnEventIndex], mobileEvents[mobileEventIndex]));
                    continue;
                }

                // We need to interpolate between two values
                if (mobileEvents[mobileEventIndex - 1].CorrectedTime < vrpnTrackerEvents[vrpnEventIndex].EventTime &&
                   mobileEvents[mobileEventIndex].CorrectedTime > vrpnTrackerEvents[vrpnEventIndex].EventTime)
                {
                    
                    double interpolatedX = (mobileEvents[mobileEventIndex - 1].Quat.X + mobileEvents[mobileEventIndex].Quat.X) / 2;
                    double interpolatedY = (mobileEvents[mobileEventIndex - 1].Quat.Y + mobileEvents[mobileEventIndex].Quat.Y) / 2;
                    double interpolatedZ = (mobileEvents[mobileEventIndex - 1].Quat.Z + mobileEvents[mobileEventIndex].Quat.Z) / 2;
                    double interpolatedW = (mobileEvents[mobileEventIndex - 1].Quat.W + mobileEvents[mobileEventIndex].Quat.W) / 2;

                    MotionEvent interpolatedEvent = new MotionEvent
                                                        {
                                                            EventTime = vrpnTrackerEvents[vrpnEventIndex].EventTime,
                                                            CorrectedTime = vrpnTrackerEvents[vrpnEventIndex].EventTime,
                                                            Quat =
                                                                new Quaternion
                                                                    {
                                                                        X = interpolatedX,
                                                                        Y = interpolatedY,
                                                                        Z = interpolatedZ,
                                                                        W = interpolatedW
                                                                    }
                                                        };

                    pairs.Add(extractDistanceFunc(vrpnTrackerEvents[vrpnEventIndex],interpolatedEvent));
                }
            }

            return pairs;
        }
    }
}
