﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TimelineAnalysis
{

    public class Statistics
    {
        // Computes the median x~ of a list of values X.
        public static double Median(List<double> values)
        {
            values.Sort();
            int size = values.Count - 1;
            if (size % 2 == 0)
            {
                // Mean between two inner-most elements
                return ((values[size / 2] + values[size / 2 - 1]) / 2);
            }
            else
            {
                return values[size / 2];
            }
        }

        public static double StandardDeviation (List<double> values)
        {
            // Sqrt( 1/n-1 * Sum(x - meanX) )

            double mean = values.Average();
            double standardDeviation = Math.Sqrt(1 / (double)(values.Count - 1) * values.Aggregate((aggregate, value) => aggregate + (value - mean) * (value - mean)));

            return standardDeviation;
        }

        public static double MedianOfAbsoluteDifferences (List<double> values)
        {
            double median = Median(values);
            List<double> listOfAbsoluteDifferences = values.Select(value => Math.Abs(value - median)).ToList();
            double MAD = Median(listOfAbsoluteDifferences);

            return MAD;
        }
    }
}
