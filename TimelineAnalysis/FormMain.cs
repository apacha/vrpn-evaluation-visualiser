﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using TimelineAnalysis.Objects;
using System.Linq;
using System.Linq.Expressions;

namespace TimelineAnalysis
{
    public partial class FormMain : Form
    {
        /// <summary>
        /// List of events from the mobile tracker (first line in the chart)
        /// </summary>
        private readonly List<MotionEvent> _mobileTrackerEvents;

        /// <summary>
        /// List of events from the VRPN tracker (second line in the chart)
        /// </summary>
        private readonly List<MotionEvent> _vrpnTrackerEvents;

        /// <summary>
        /// The list of all charts
        /// </summary>
        private readonly List<Chart> _chartList = new List<Chart>();

        /// <summary>
        /// The label that will be displayed in the chart legend for the first line
        /// </summary>
        private const string MobileTrackerLabel = "Mobile Device";

        /// <summary>
        /// The label that will be displayed in the chart legend for the second line
        /// </summary>
        private const string VrpnTrackerLabel = "VRPN Tracker";

        /// <summary>
        /// The label that will be displayed in the chart legend for the sync
        /// </summary>
        private const string TimeSynchroniseLabel = "Time Sync";

        private readonly Color _colourTracker = Color.Green;
        private readonly Color _colourMobile = Color.Blue;
        private readonly Color _colourTimeSync = Color.Red;

        private double _timeSync = 0;

        /// <summary>
        /// Initialise all lists
        /// </summary>
        public FormMain()
        {
            InitializeComponent();
            _mobileTrackerEvents = new List<MotionEvent>();
            _vrpnTrackerEvents = new List<MotionEvent>();
            _chartList.Add(chart1);
            _chartList.Add(chart2);
            _chartList.Add(chart3);
            _chartList.Add(chart4);
        }

        private void ButtonSelectMobilePathClick(object sender, EventArgs e)
        {
            try
            {
                var dialog = new OpenFileDialog
                   {
                       CheckFileExists = true,
                       Multiselect = false,
                   };
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    textBox1.Text = dialog.FileName;
                }
            }
            catch (IOException exception)
            {
                MessageBox.Show("Error when loading data: " + exception.Message, "Exception");
            }
        }
        private void ButtonSelectVprnPathClick(object sender, EventArgs e)
        {
            try
            {
                var dialog = new OpenFileDialog
                {
                    CheckFileExists = true,
                    Multiselect = false,
                };
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    textBox2.Text = dialog.FileName;
                }
            }
            catch (IOException exception)
            {
                MessageBox.Show("Error when loading data: " + exception.Message, "Exception");
            }
        }


        private void ButtonLoadDataFromMobileClick(object sender, EventArgs e)
        {
            if (!File.Exists(textBox1.Text))
            {
                MessageBox.Show("Could not find file: " + textBox1.Text);
                return;
            }

            _mobileTrackerEvents.Clear();
            var reader = new StreamReader(textBox1.Text);
            string line;
            while ((line = reader.ReadLine()) != null)
            {
                _mobileTrackerEvents.Add(MotionEvent.InitialiseFromString(line));
                //Console.WriteLine(line);
            }
            reader.Close();
            UpdateChart();

            Console.WriteLine("Load Mobile data");
        }

        private void ButtonLoadDataFromVprnClick(object sender, EventArgs e)
        {
            if (!File.Exists(textBox2.Text))
            {
                MessageBox.Show("Could not find file: " + textBox1.Text);
                return;
            }

            _vrpnTrackerEvents.Clear();
            var reader = new StreamReader(textBox2.Text);
            string line;
            while ((line = reader.ReadLine()) != null)
            {
                _vrpnTrackerEvents.Add(MotionEvent.InitialiseFromString(line));
                //Console.WriteLine(line);
            }
            reader.Close();
            UpdateChart();
        }

        private void UpdateChart()
        {
            foreach (var chart in _chartList)
            {
                chart.Series.Clear();
                chart.Series.Add(MobileTrackerLabel);
                chart.Series.Add(VrpnTrackerLabel);
                chart.Series.Add(TimeSynchroniseLabel);
                chart.Series[MobileTrackerLabel].ChartType = SeriesChartType.FastLine;
                chart.Series[MobileTrackerLabel].Color = _colourMobile;
                chart.Series[VrpnTrackerLabel].ChartType = SeriesChartType.FastLine;
                chart.Series[VrpnTrackerLabel].Color = _colourTracker;
                chart.Series[TimeSynchroniseLabel].ChartType = SeriesChartType.FastLine;
                chart.Series[TimeSynchroniseLabel].Color = _colourTimeSync;
                chart.Series[TimeSynchroniseLabel].Points.AddXY(_timeSync, -1);
                chart.Series[TimeSynchroniseLabel].Points.AddXY(_timeSync, 1);
            }

            foreach (var mobileEvent in _mobileTrackerEvents)
            {
                chart1.Series[MobileTrackerLabel].Points.AddXY(mobileEvent.CorrectedTime, mobileEvent.Quat.X);
                chart2.Series[MobileTrackerLabel].Points.AddXY(mobileEvent.CorrectedTime, mobileEvent.Quat.Y);
                chart3.Series[MobileTrackerLabel].Points.AddXY(mobileEvent.CorrectedTime, mobileEvent.Quat.Z);
                chart4.Series[MobileTrackerLabel].Points.AddXY(mobileEvent.CorrectedTime, mobileEvent.Quat.W);
            }

            foreach (var trackerEvent in _vrpnTrackerEvents)
            {
                chart1.Series[VrpnTrackerLabel].Points.AddXY(trackerEvent.CorrectedTime, trackerEvent.Quat.X);
                chart2.Series[VrpnTrackerLabel].Points.AddXY(trackerEvent.CorrectedTime, trackerEvent.Quat.Y);
                chart3.Series[VrpnTrackerLabel].Points.AddXY(trackerEvent.CorrectedTime, trackerEvent.Quat.Z);
                chart4.Series[VrpnTrackerLabel].Points.AddXY(trackerEvent.CorrectedTime, trackerEvent.Quat.W);
            }


        }
        private void TrackbarTimeshiftScroll(object sender, EventArgs e)
        {
            labelTimeShift.Text = "Timeshift: " + trackBarTimeShift.Value;
            foreach (var mobileEvent in _mobileTrackerEvents)
            {
                mobileEvent.CorrectedTime = mobileEvent.EventTime + trackBarTimeShift.Value / 10.0;
            }

            UpdateChart();
        }

        private void ButtonNormaliseQuaternionClick(object sender, EventArgs e)
        {
            _timeSync = Double.Parse(textBoxTimeSync.Text);
            Quaternion quat1 = null;
            double minDiff = double.MaxValue;
            Quaternion quat2 = null;

            foreach (var mobileEvent in _mobileTrackerEvents)
            {
                double diff = Math.Abs(mobileEvent.CorrectedTime - _timeSync);
                if (diff < minDiff)
                {
                    // Close event found
                    quat1 = mobileEvent.Quat;
                    minDiff = diff;
                }
            }

            minDiff = double.MaxValue;

            foreach (var trackerEvent in _vrpnTrackerEvents)
            {
                double diff = Math.Abs(trackerEvent.CorrectedTime - _timeSync);
                if (diff < minDiff)
                {
                    // Close event found
                    quat2 = trackerEvent.Quat;
                    minDiff = diff;
                }
            }

            var quatTransform = new Quaternion();
            quat1.Invert().Multiply(quat2, quatTransform);

            var verify = new Quaternion();
            quat1.Multiply(quatTransform, verify);

            foreach (var mobileEvent in _mobileTrackerEvents)
            {
                Quaternion result = new Quaternion();
                mobileEvent.Quat.Multiply(quatTransform, result);
                mobileEvent.Quat = result;
            }

            UpdateChart();

            Console.WriteLine("Normalise " + _timeSync);
        }

        private void ButtonInvertXClick(object sender, EventArgs e)
        {
            foreach (var mobileEvent in _mobileTrackerEvents)
            {
                mobileEvent.Quat.X = -mobileEvent.Quat.X;
            }

            UpdateChart();
            Console.WriteLine("Invert X");
        }

        private void ButtonInvertYClick(object sender, EventArgs e)
        {
            foreach (var mobileEvent in _mobileTrackerEvents)
            {
                mobileEvent.Quat.Y = -mobileEvent.Quat.Y;
            }

            UpdateChart();
            Console.WriteLine("Invert Y");
        }

        private void ButtonInvertZClick(object sender, EventArgs e)
        {
            foreach (var mobileEvent in _mobileTrackerEvents)
            {
                mobileEvent.Quat.Z = -mobileEvent.Quat.Z;
            }

            UpdateChart();
            Console.WriteLine("Invert Z");
        }

        private void ButtonSwapXyClick(object sender, EventArgs e)
        {
            foreach (var mobileEvent in _mobileTrackerEvents)
            {
                double x = mobileEvent.Quat.X;
                mobileEvent.Quat.X = mobileEvent.Quat.Y;
                mobileEvent.Quat.Y = x;
            }

            UpdateChart();
            Console.WriteLine("Swap XY");
        }

        private void ButtonSwapYzClick(object sender, EventArgs e)
        {
            foreach (var mobileEvent in _mobileTrackerEvents)
            {
                double y = mobileEvent.Quat.Y;
                mobileEvent.Quat.Y = mobileEvent.Quat.Z;
                mobileEvent.Quat.Z = y;
            }

            UpdateChart();
            Console.WriteLine("Swap YZ");
        }

        private void ButtonCalculateEvaluationClick(object sender, EventArgs e)
        {
            Func<MotionEvent, MotionEvent, double> euclideanDistanceFunc = (event1, event2) => Math.Sqrt((event1.Quat.X - event2.Quat.X) *
                                                                                                     (event1.Quat.X - event2.Quat.X) +
                                                                                                     (event1.Quat.Y - event2.Quat.Y) *
                                                                                                     (event1.Quat.Y - event2.Quat.Y) +
                                                                                                     (event1.Quat.Z - event2.Quat.Z) *
                                                                                                     (event1.Quat.Z - event2.Quat.Z) +
                                                                                                     (event1.Quat.W - event2.Quat.W) *
                                                                                                     (event1.Quat.W - event2.Quat.W));

            Func<MotionEvent, MotionEvent, double> angleDistanceFunc = (event1, event2) =>
                                                                           {
                                                                               Quaternion output = new Quaternion();
                                                                               Quaternion q1 = event1.Quat.Clone();
                                                                               q1.Invert().Multiply(event2.Quat, output);
                                                                               AxisAngle axisAngle = AxisAngle.InitialiseFromQuaternion(output);
                                                                               return axisAngle.Angle;
                                                                           };

            EvaluationAlgorithm evaluation = new EvaluationAlgorithm();
            List<double> euclideanPairs = evaluation.EvaluatePerformance(_vrpnTrackerEvents, _mobileTrackerEvents, euclideanDistanceFunc);
            var orderedElements1 = euclideanPairs.OrderByDescending(x => x).ToList();
            double average1 = orderedElements1.Average();

            List<double> anglePairs = evaluation.EvaluatePerformance(_vrpnTrackerEvents, _mobileTrackerEvents, angleDistanceFunc, Double.Parse(textBox3.Text));
            var orderedElements2 = anglePairs.OrderByDescending(x => x).ToList();           
            double average2 = orderedElements2.Average();
            double median2 = Statistics.Median(orderedElements2);
            double sd2 = Statistics.StandardDeviation(orderedElements2);
            double mad2 = Statistics.MedianOfAbsoluteDifferences(orderedElements2);

            MessageBox.Show(String.Format("Avg: {0}\nSD: {1}\nMedian: {2}\nMAD: {3}",average2,median2,sd2,mad2));
        }
    }
}
