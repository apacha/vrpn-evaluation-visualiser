﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TimelineAnalysis.Objects
{
    /// <summary>
    /// Object that contains an axis and an angle as representation for an arbitrary rotation
    /// </summary>
    public class AxisAngle
    {
        public double AxisX { get; set; }
        public double AxisY { get; set; }
        public double AxisZ { get; set; }
        public double Angle { get; set; }

        public static AxisAngle InitialiseFromQuaternion(Quaternion q)
        {
            if (q.W > 1)
                q.Normalise(); // if w>1 acos and sqrt will produce errors, this cant happen if quaternion is normalised
            double angle = Math.Acos(q.W) * 180d / Math.PI;
            double x;
            double y;
            double z;

            double s = (float)Math.Sqrt(1 - q.W * q.W); // assuming quaternion normalised then w is less than 1, so term always positive.
            if (s < 0.001)
            { // test to avoid divide by zero, s is always positive due to sqrt
                // if s close to zero then direction of axis not important
                x = q.X; // if it is important that axis is normalised then replace with x=1; y=z=0;
                y = q.Y;
                z = q.Z;
            }
            else
            {
                x = q.X / s; // normalise axis
                y = q.Y / s;
                z = q.Z / s;
            }

            return new AxisAngle
                       {
                           AxisX = x,
                           AxisY = y,
                           AxisZ = z,
                           Angle = angle
                       };
        }
    }


}
