﻿namespace TimelineAnalysis.Objects
{
    /// <summary>
    /// A motion event as read from a file
    /// </summary>
    public class MotionEvent
    {
        /// <summary>
        /// The time, when the event happened (in milliseconds after start of tracking)
        /// </summary>
        public double EventTime { get; set; }

        /// <summary>
        /// The time, when the event happened (in milliseconds after start of tracking)
        /// </summary>
        public double CorrectedTime { get; set; }

        /// <summary>
        /// The Quaternion as read from the tracking-file
        /// </summary>
        public Quaternion Quat { get; set; }

        /// <summary>
        /// The rotation represented in euler angles (azimuth, pitch, roll)
        /// </summary>
        public EulerAngles Angles { get; set; }

        /// <summary>
        /// The rotation represented as axis and angle
        /// </summary>
        public AxisAngle RotationVector { get; set; }

        /// <summary>
        /// Create a new Motion-Event and initialise it from a string of this format: "Time:5.005;Quaternion:0.166,-0.206,-0.003,0.964"
        /// </summary>
        /// <param name="eventString">A string from the tracker-file in the format: "Time:5.005;Quaternion:0.166,-0.206,-0.003,0.964"</param>
        /// <returns>A new MotionEvent object initialised with the values from the string provided</returns>
        public static MotionEvent InitialiseFromString(string eventString)
        {
            double time = double.Parse(eventString.Split(';')[0].Split(':')[1]);
            double x = double.Parse(eventString.Split(';')[1].Split(':')[1].Split(',')[0]);
            double y = double.Parse(eventString.Split(';')[1].Split(':')[1].Split(',')[1]);
            double z = double.Parse(eventString.Split(';')[1].Split(':')[1].Split(',')[2]);
            double w = double.Parse(eventString.Split(';')[1].Split(':')[1].Split(',')[3]);
            var quat = new Quaternion { X = x, Y = y, Z = z, W = w };
            return new MotionEvent
                       {
                           EventTime = time, 
                           CorrectedTime = time, 
                           Quat = quat, Angles = EulerAngles.InitialiseFromQuaternion(quat),
                           RotationVector = AxisAngle.InitialiseFromQuaternion(quat)
                       };
        }



        public override string ToString()
        {
            return string.Format("MotionEvent[EventTime: {0}, CorrectedTime: {1}, Quat: {2}, Angles: {3}, RotationVector: {4}]", EventTime, CorrectedTime, Quat, Angles, RotationVector);
        }
    }
}