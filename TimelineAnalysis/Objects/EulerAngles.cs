﻿using System;

namespace TimelineAnalysis.Objects
{
    public class EulerAngles
    {
        public double Azimuth { get; set; }
        public double Pitch { get; set; }
        public double Roll { get; set; }

        public static EulerAngles InitialiseFromQuaternion(Quaternion q)
        {
            return new EulerAngles
                       {
                           Azimuth = Math.Atan(2*(q.W*q.Z + q.X*q.Y)/(1 - 2*(q.Y*q.Y + q.Z*q.Z))),
                           Pitch = Math.Asin(2*(q.W*q.Y - q.Z*q.X)),
                           Roll = Math.Atan(2*(q.W*q.X + q.Y*q.Z)/(1 - 2*(q.X*q.X + q.Y*q.Y)))
                       };
        }
    }
}