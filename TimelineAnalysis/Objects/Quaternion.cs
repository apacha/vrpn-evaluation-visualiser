﻿using System;

namespace TimelineAnalysis.Objects
{
    /// <summary>
    /// The Quaternion storage class
    /// </summary>
    public class Quaternion
    {
        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }
        public double W { get; set; }

        /// <summary>
        /// Initialises a new quaternion with the identity quaternion
        /// </summary>
        public Quaternion()
        {
            X = Y = Z = 0;
            W = 1;
        }

        public Quaternion Clone()
        {
            return new Quaternion { X = X, Y = Y, Z = Z, W = W };
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </returns>
        /// <filterpriority>2</filterpriority>
        public override string ToString()
        {
            return String.Format("Quaternion[X:{0},Y:{1},Z:{2},W:{3}]", X,Y,Z,W);
        }

        public void Normalise()
        {
            double mag = (float)Math.Sqrt(W * W + X * X + Y * Y + Z * Z);
            W /= mag;
            X /= mag;
            Y /= mag;
            Z /= mag;
        }

        public Quaternion Conjugate()
        {
            return new Quaternion()
                       {
                           X = -X,
                           Y = -Y,
                           Z = -Z,
                           W = W,
                       };
        }

        public Quaternion Invert()
        {
            double mag = X*X + Y*Y + Z*Z + W*W;
            return new Quaternion()
            {
                X = -X / mag,
                Y = -Y / mag,
                Z = -Z / mag,
                W = W / mag,
            };
        }

        public void Multiply(Quaternion input, Quaternion output)
        {
            
            if (input != output)
            {
                output.W = (W * input.W - X * input.X - Y * input.Y - Z
                        * input.Z); //w = w1w2 - x1x2 - y1y2 - z1z2
                output.X = (W * input.X + X * input.W + Y * input.Z - Z
                        * input.Y); //x = w1x2 + x1w2 + y1z2 - z1y2
                output.Y = (W * input.Y + Y * input.W + Z * input.X - X
                        * input.Z); //y = w1y2 + y1w2 + z1x2 - x1z2
                output.Z = (W * input.Z + Z * input.W + X * input.Y - Y
                        * input.X); //z = w1z2 + z1w2 + x1y2 - y1x2
            }
        }
    }

}