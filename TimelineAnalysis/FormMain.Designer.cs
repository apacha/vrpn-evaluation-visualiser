﻿namespace TimelineAnalysis
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend4 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.buttonLoadDataFromMobile = new System.Windows.Forms.Button();
            this.buttonLoadDataFromVPRN = new System.Windows.Forms.Button();
            this.buttonSelectMobilePath = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.buttonSelectVPRNPath = new System.Windows.Forms.Button();
            this.trackBarTimeShift = new System.Windows.Forms.TrackBar();
            this.labelTimeShift = new System.Windows.Forms.Label();
            this.chart2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart3 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart4 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.buttonNormQuat = new System.Windows.Forms.Button();
            this.buttonInvertX = new System.Windows.Forms.Button();
            this.buttonInvertY = new System.Windows.Forms.Button();
            this.buttonInvertZ = new System.Windows.Forms.Button();
            this.buttonSwapXY = new System.Windows.Forms.Button();
            this.buttonSwapYZ = new System.Windows.Forms.Button();
            this.textBoxTimeSync = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonCalculateEvaluation = new System.Windows.Forms.Button();
            this.textBox3 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarTimeShift)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart4)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // chart1
            // 
            this.chart1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(6, 6);
            this.chart1.Name = "chart1";
            this.chart1.Size = new System.Drawing.Size(658, 131);
            this.chart1.TabIndex = 0;
            this.chart1.Text = "chart1";
            // 
            // buttonLoadDataFromMobile
            // 
            this.buttonLoadDataFromMobile.Location = new System.Drawing.Point(12, 12);
            this.buttonLoadDataFromMobile.Name = "buttonLoadDataFromMobile";
            this.buttonLoadDataFromMobile.Size = new System.Drawing.Size(163, 23);
            this.buttonLoadDataFromMobile.TabIndex = 1;
            this.buttonLoadDataFromMobile.Text = "Load Mobile Tracker data";
            this.buttonLoadDataFromMobile.UseVisualStyleBackColor = true;
            this.buttonLoadDataFromMobile.Click += new System.EventHandler(this.ButtonLoadDataFromMobileClick);
            // 
            // buttonLoadDataFromVPRN
            // 
            this.buttonLoadDataFromVPRN.Location = new System.Drawing.Point(12, 41);
            this.buttonLoadDataFromVPRN.Name = "buttonLoadDataFromVPRN";
            this.buttonLoadDataFromVPRN.Size = new System.Drawing.Size(163, 23);
            this.buttonLoadDataFromVPRN.TabIndex = 2;
            this.buttonLoadDataFromVPRN.Text = "Load VPRN Tracker data";
            this.buttonLoadDataFromVPRN.UseVisualStyleBackColor = true;
            this.buttonLoadDataFromVPRN.Click += new System.EventHandler(this.ButtonLoadDataFromVprnClick);
            // 
            // buttonSelectMobilePath
            // 
            this.buttonSelectMobilePath.Location = new System.Drawing.Point(661, 12);
            this.buttonSelectMobilePath.Name = "buttonSelectMobilePath";
            this.buttonSelectMobilePath.Size = new System.Drawing.Size(27, 23);
            this.buttonSelectMobilePath.TabIndex = 3;
            this.buttonSelectMobilePath.Text = "...";
            this.buttonSelectMobilePath.UseVisualStyleBackColor = true;
            this.buttonSelectMobilePath.Click += new System.EventHandler(this.ButtonSelectMobilePathClick);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(195, 14);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(460, 20);
            this.textBox1.TabIndex = 4;
            this.textBox1.Text = "C:\\Users\\Alex\\Documents\\Visual Studio 2010\\Projects\\VRPN-Evaluation Tool\\Data\\Sma" +
    "rtphonesSession3\\SensorFusion2.txt";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(195, 43);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(460, 20);
            this.textBox2.TabIndex = 5;
            this.textBox2.Text = "C:\\Users\\Alex\\Documents\\Visual Studio 2010\\Projects\\VRPN-Evaluation Tool\\Data\\Sma" +
    "rtphonesSession3\\VRPN-Tracker.txt";
            // 
            // buttonSelectVPRNPath
            // 
            this.buttonSelectVPRNPath.Location = new System.Drawing.Point(661, 41);
            this.buttonSelectVPRNPath.Name = "buttonSelectVPRNPath";
            this.buttonSelectVPRNPath.Size = new System.Drawing.Size(27, 23);
            this.buttonSelectVPRNPath.TabIndex = 6;
            this.buttonSelectVPRNPath.Text = "...";
            this.buttonSelectVPRNPath.UseVisualStyleBackColor = true;
            this.buttonSelectVPRNPath.Click += new System.EventHandler(this.ButtonSelectVprnPathClick);
            // 
            // trackBarTimeShift
            // 
            this.trackBarTimeShift.Location = new System.Drawing.Point(98, 69);
            this.trackBarTimeShift.Maximum = 100;
            this.trackBarTimeShift.Minimum = -100;
            this.trackBarTimeShift.Name = "trackBarTimeShift";
            this.trackBarTimeShift.Size = new System.Drawing.Size(282, 45);
            this.trackBarTimeShift.TabIndex = 7;
            this.trackBarTimeShift.Scroll += new System.EventHandler(this.TrackbarTimeshiftScroll);
            // 
            // labelTimeShift
            // 
            this.labelTimeShift.AutoSize = true;
            this.labelTimeShift.Location = new System.Drawing.Point(12, 73);
            this.labelTimeShift.Name = "labelTimeShift";
            this.labelTimeShift.Size = new System.Drawing.Size(52, 13);
            this.labelTimeShift.TabIndex = 8;
            this.labelTimeShift.Text = "Timeshift:";
            // 
            // chart2
            // 
            this.chart2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            chartArea2.Name = "ChartArea1";
            this.chart2.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.chart2.Legends.Add(legend2);
            this.chart2.Location = new System.Drawing.Point(6, 143);
            this.chart2.Name = "chart2";
            this.chart2.Size = new System.Drawing.Size(658, 146);
            this.chart2.TabIndex = 10;
            this.chart2.Text = "chart2";
            // 
            // chart3
            // 
            this.chart3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            chartArea3.Name = "ChartArea1";
            this.chart3.ChartAreas.Add(chartArea3);
            legend3.Name = "Legend1";
            this.chart3.Legends.Add(legend3);
            this.chart3.Location = new System.Drawing.Point(6, 295);
            this.chart3.Name = "chart3";
            this.chart3.Size = new System.Drawing.Size(658, 160);
            this.chart3.TabIndex = 11;
            this.chart3.Text = "chart3";
            // 
            // chart4
            // 
            this.chart4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            chartArea4.Name = "ChartArea1";
            this.chart4.ChartAreas.Add(chartArea4);
            legend4.Name = "Legend1";
            this.chart4.Legends.Add(legend4);
            this.chart4.Location = new System.Drawing.Point(6, 461);
            this.chart4.Name = "chart4";
            this.chart4.Size = new System.Drawing.Size(658, 152);
            this.chart4.TabIndex = 12;
            this.chart4.Text = "chart4";
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Location = new System.Drawing.Point(12, 106);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(678, 640);
            this.tabControl1.TabIndex = 13;
            // 
            // tabPage1
            // 
            this.tabPage1.AutoScroll = true;
            this.tabPage1.Controls.Add(this.chart1);
            this.tabPage1.Controls.Add(this.chart4);
            this.tabPage1.Controls.Add(this.chart2);
            this.tabPage1.Controls.Add(this.chart3);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(670, 614);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Quaternions";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // buttonNormQuat
            // 
            this.buttonNormQuat.Location = new System.Drawing.Point(491, 96);
            this.buttonNormQuat.Name = "buttonNormQuat";
            this.buttonNormQuat.Size = new System.Drawing.Size(68, 23);
            this.buttonNormQuat.TabIndex = 15;
            this.buttonNormQuat.Text = "Norm Quat";
            this.buttonNormQuat.UseVisualStyleBackColor = true;
            this.buttonNormQuat.Click += new System.EventHandler(this.ButtonNormaliseQuaternionClick);
            // 
            // buttonInvertX
            // 
            this.buttonInvertX.Location = new System.Drawing.Point(386, 68);
            this.buttonInvertX.Name = "buttonInvertX";
            this.buttonInvertX.Size = new System.Drawing.Size(43, 23);
            this.buttonInvertX.TabIndex = 16;
            this.buttonInvertX.Text = "Inv X";
            this.buttonInvertX.UseVisualStyleBackColor = true;
            this.buttonInvertX.Click += new System.EventHandler(this.ButtonInvertXClick);
            // 
            // buttonInvertY
            // 
            this.buttonInvertY.Location = new System.Drawing.Point(435, 68);
            this.buttonInvertY.Name = "buttonInvertY";
            this.buttonInvertY.Size = new System.Drawing.Size(47, 23);
            this.buttonInvertY.TabIndex = 17;
            this.buttonInvertY.Text = "Inv Y";
            this.buttonInvertY.UseVisualStyleBackColor = true;
            this.buttonInvertY.Click += new System.EventHandler(this.ButtonInvertYClick);
            // 
            // buttonInvertZ
            // 
            this.buttonInvertZ.Location = new System.Drawing.Point(484, 68);
            this.buttonInvertZ.Name = "buttonInvertZ";
            this.buttonInvertZ.Size = new System.Drawing.Size(41, 23);
            this.buttonInvertZ.TabIndex = 18;
            this.buttonInvertZ.Text = "Inv Z";
            this.buttonInvertZ.UseVisualStyleBackColor = true;
            this.buttonInvertZ.Click += new System.EventHandler(this.ButtonInvertZClick);
            // 
            // buttonSwapXY
            // 
            this.buttonSwapXY.Location = new System.Drawing.Point(532, 68);
            this.buttonSwapXY.Name = "buttonSwapXY";
            this.buttonSwapXY.Size = new System.Drawing.Size(75, 23);
            this.buttonSwapXY.TabIndex = 19;
            this.buttonSwapXY.Text = "Swap X-Y";
            this.buttonSwapXY.UseVisualStyleBackColor = true;
            this.buttonSwapXY.Click += new System.EventHandler(this.ButtonSwapXyClick);
            // 
            // buttonSwapYZ
            // 
            this.buttonSwapYZ.Location = new System.Drawing.Point(613, 68);
            this.buttonSwapYZ.Name = "buttonSwapYZ";
            this.buttonSwapYZ.Size = new System.Drawing.Size(75, 23);
            this.buttonSwapYZ.TabIndex = 23;
            this.buttonSwapYZ.Text = "Swap Y-Z";
            this.buttonSwapYZ.UseVisualStyleBackColor = true;
            this.buttonSwapYZ.Click += new System.EventHandler(this.ButtonSwapYzClick);
            // 
            // textBoxTimeSync
            // 
            this.textBoxTimeSync.Location = new System.Drawing.Point(447, 98);
            this.textBoxTimeSync.Name = "textBoxTimeSync";
            this.textBoxTimeSync.Size = new System.Drawing.Size(35, 20);
            this.textBoxTimeSync.TabIndex = 24;
            this.textBoxTimeSync.Text = "19";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(385, 101);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 25;
            this.label1.Text = "Norm time:";
            // 
            // buttonCalculateEvaluation
            // 
            this.buttonCalculateEvaluation.Location = new System.Drawing.Point(613, 96);
            this.buttonCalculateEvaluation.Name = "buttonCalculateEvaluation";
            this.buttonCalculateEvaluation.Size = new System.Drawing.Size(75, 23);
            this.buttonCalculateEvaluation.TabIndex = 26;
            this.buttonCalculateEvaluation.Text = "Evaluate";
            this.buttonCalculateEvaluation.UseVisualStyleBackColor = true;
            this.buttonCalculateEvaluation.Click += new System.EventHandler(this.ButtonCalculateEvaluationClick);
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(566, 97);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(41, 20);
            this.textBox3.TabIndex = 27;
            this.textBox3.Text = "200";
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(700, 769);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.buttonCalculateEvaluation);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxTimeSync);
            this.Controls.Add(this.buttonSwapYZ);
            this.Controls.Add(this.buttonSwapXY);
            this.Controls.Add(this.buttonInvertZ);
            this.Controls.Add(this.buttonInvertY);
            this.Controls.Add(this.buttonInvertX);
            this.Controls.Add(this.buttonNormQuat);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.labelTimeShift);
            this.Controls.Add(this.trackBarTimeShift);
            this.Controls.Add(this.buttonSelectVPRNPath);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.buttonSelectMobilePath);
            this.Controls.Add(this.buttonLoadDataFromVPRN);
            this.Controls.Add(this.buttonLoadDataFromMobile);
            this.Name = "FormMain";
            this.Text = "Analyser";
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarTimeShift)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart4)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.Button buttonLoadDataFromMobile;
        private System.Windows.Forms.Button buttonLoadDataFromVPRN;
        private System.Windows.Forms.Button buttonSelectMobilePath;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button buttonSelectVPRNPath;
        private System.Windows.Forms.TrackBar trackBarTimeShift;
        private System.Windows.Forms.Label labelTimeShift;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart2;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart3;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart4;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button buttonNormQuat;
        private System.Windows.Forms.Button buttonInvertX;
        private System.Windows.Forms.Button buttonInvertY;
        private System.Windows.Forms.Button buttonInvertZ;
        private System.Windows.Forms.Button buttonSwapXY;
        private System.Windows.Forms.Button buttonSwapYZ;
        private System.Windows.Forms.TextBox textBoxTimeSync;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonCalculateEvaluation;
        private System.Windows.Forms.TextBox textBox3;
    }
}

