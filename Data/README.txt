This directory contains the data sets of 13 different recording-sessions, recorded with different devices and different motions.

All recordings were performed manually by holding the VRPN-marker tightly onto the device that was tracking the rotation with different fusion-algorithms.

SmartphoneSessions were recorded with a Samsung Galaxy S2, TabletSessions with a Samsung Galaxy Tab 10.1, Glasses were recorded with Google Glasses head-mounted and GlassesInHand were recorded with Google Glasses but hand-held.

Smartphone Session 5 also contains repetitive camera-captures to demonstrate the motion that the device was undergoing.

The motion generally started with walking to the middle of the tracker, then looking left, right, up and down. Then performing skewed sideway-rotations followed by medium up and down shaking. A heavy shaking in form of an eight that causes every tracker to fail was done next. After this, reinitialisation was automatically done and finally some up-down shaking was done to create clear spikes in both datasets.