﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TimelineAnalysis.Objects;

namespace TimelineAnalysisTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMotionEventInitilise()
        {
            MotionEvent m = MotionEvent.InitialiseFromString("Time:5.005;Quaternion:0.166,-0.206,-0.003,0.964");

            Assert.AreEqual(5.005, m.EventTime);
            Assert.AreEqual(0.166, m.Quat.X);
            Assert.AreEqual(-0.206, m.Quat.Y);
            Assert.AreEqual(-0.003, m.Quat.Z);
            Assert.AreEqual(0.964, m.Quat.W);
        }

        [TestMethod]
        public void TestMotionEventInitiliseWithSpaces()
        {
            MotionEvent m = MotionEvent.InitialiseFromString("Time :5.005; Quaternion: 0.166, -0.206, -0.003,0.964");

            Assert.AreEqual(5.005, m.EventTime);
            Assert.AreEqual(0.166, m.Quat.X);
            Assert.AreEqual(-0.206, m.Quat.Y);
            Assert.AreEqual(-0.003, m.Quat.Z);
            Assert.AreEqual(0.964, m.Quat.W);
        }

        [TestMethod]
        public void TestQuaternionInversion()
        {
            Quaternion m = new Quaternion()
                               {
                                   X = 0.5,
                                   Y = 0.1,
                                   Z = 0.4,
                                   W = 0.3
                               };
            Quaternion mInv = m.Invert();

            var output = new Quaternion();
            Assert.AreEqual(1, output.W);
            Assert.AreEqual(0, output.X);
            Assert.AreEqual(0, output.Y);
            Assert.AreEqual(0, output.Z);
            mInv.Multiply(m, output);

            Assert.IsTrue((1-output.W) < 0.000001);
            Assert.IsTrue((0 - output.X) < 0.000001);
            Assert.IsTrue((0 - output.Y) < 0.000001);
            Assert.IsTrue((0 - output.Z) < 0.000001);
            
        }
    }
}
